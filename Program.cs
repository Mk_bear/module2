using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public static double ParseToDouble(string value)
        {
            double result = Double.NaN;
            value = value.Trim();
            if (!double.TryParse(value, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.GetCultureInfo("ru-RU"), out result))
            {
                if (!double.TryParse(value, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.GetCultureInfo("en-US"), out result))
                {
                    return Double.NaN;
                }
            }
            return result;
        }
        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            return companyRevenue * companiesNumber * (tax / 100);
            throw new NotImplementedException();
        }

        public string GetCongratulation(int input)
        {
            if (input % 2 == 0 && input >= 18)
                return "Поздравляю с совершеннолетием!";
            else if (input % 2 != 0 && input > 12 & input < 18)
                return "Поздравляю с переходом в старшую школу!";
            else
                return "Поздравляю c " +input + "-летием!";
            throw new NotImplementedException();
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            if (ParseToDouble(first) == Double.NaN || ParseToDouble(second) == Double.NaN)
                return Double.NaN;
            else
                return ParseToDouble(first) * ParseToDouble(second);
            throw new NotImplementedException();
        }

        public double GetFigureValues(FigureEnum figureType, ParameterEnum parameterToCompute, Dimensions dimensions)
        {
            if (parameterToCompute == ParameterEnum.Square && figureType == FigureEnum.Rectangle)
                return Math.Truncate(dimensions.FirstSide * dimensions.SecondSide);
            else if (parameterToCompute == ParameterEnum.Perimeter && figureType == FigureEnum.Rectangle)
                return Math.Truncate((dimensions.FirstSide + dimensions.SecondSide) * 2);
            else if (parameterToCompute == ParameterEnum.Square && figureType == FigureEnum.Triangle)
                return Math.Truncate(0.5 * dimensions.Height * dimensions.FirstSide);
            else if (parameterToCompute == ParameterEnum.Perimeter && figureType == FigureEnum.Triangle)
                return Math.Truncate(dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide);
            else if (parameterToCompute == ParameterEnum.Perimeter && figureType == FigureEnum.Circle)
                return Math.Round(Math.PI * dimensions.Diameter);
            else
                return Math.Round(Math.PI * Math.Pow(dimensions.Radius, 2));
                throw new NotImplementedException();
        }
    }
}
